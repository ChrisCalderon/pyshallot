#!/usr/bin/env python3.7
from subprocess import run, DEVNULL
from multiprocessing import cpu_count
import argparse
from time import perf_counter
import sys

parser = argparse.ArgumentParser(
    description='Benchmark the shallot module.'
)
parser.add_argument(
    '-t', '--threads',
    help='The number of processes to run.',
    type=int,
    default=cpu_count()
)
parser.add_argument(
    '-l', '--max-prefix-length',
    help=(
        'The maximum prefix length to try. '
        'Any longer than 6 will take a very long time.'
    ),
    type=int,
    default=5,
)
parser.add_argument(
    '-T', '--trials',
    help=(
        'The number of trials per length.'
        'More trials gives a more accurate average but will take longer.'
    ),
    type=int,
    default=10
)


def main():
    args = parser.parse_args()
    threads = str(args.threads)
    trials = args.trials
    for length in range(1, args.max_prefix_length + 1):
        prefix = '^' + 'a'*length
        args = ['python', '-m', 'shallot', '-p', prefix, '-t', threads]
        start = perf_counter()
        for trial in range(trials):
            run(args, stdout=DEVNULL)
        end = perf_counter()
        print(f'Average time for prefix of length {length}:',
              (end - start)/trials)

    return 0


if __name__ == '__main__':
    sys.exit(main())
