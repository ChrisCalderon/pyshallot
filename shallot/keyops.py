from typing import Iterator, Tuple
from OpenSSL.crypto import (
    PKey, TYPE_RSA,
    dump_privatekey, load_privatekey,
    FILETYPE_ASN1, FILETYPE_PEM
)
from asn1crypto.keys import RSAPrivateKey, RSAPublicKey
from shallot.safemath import invert
from hashlib import sha1
from base64 import b32encode


def encode_onion(modulus: int, public_exponent: int) -> bytes:
    """Encodes a public key as an onion domain."""
    pubkey = RSAPublicKey({
        'modulus': modulus,
        'public_exponent': public_exponent
    })
    pubkey_hash = sha1(pubkey.dump()).digest()
    return b32encode(pubkey_hash)[:16].lower()


def check(modulus: int,
          public_exponent: int,
          private_exponent: int,
          prime1: int,
          prime2: int) -> bytes:
    """Checks consistency via OpenSSL and returns the PEM encoded private key."""
    private_key = RSAPrivateKey({
        'version': 0,
        'modulus': modulus,
        'public_exponent': public_exponent,
        'private_exponent': private_exponent,
        'prime1': prime1,
        'prime2': prime2,
        'exponent1': private_exponent % (prime1 - 1),
        'exponent2': private_exponent % (prime2 - 1),
        'coefficient': invert(prime2, prime1)
    })
    dump = private_key.dump()
    private_key_copy = load_privatekey(FILETYPE_ASN1, dump)
    if private_key_copy.check():
        return dump_privatekey(FILETYPE_PEM, private_key_copy)
    else:
        return b''


def gen_key_bases(bits: int) -> Iterator[Tuple[int, int, int]]:
    """Generates an endless stream of prime1, prime2, modulus."""
    while True:
        private_key = PKey()
        private_key.generate_key(TYPE_RSA, bits)
        dump = dump_privatekey(FILETYPE_ASN1, private_key)
        private_key_copy = RSAPrivateKey.load(dump)
        yield (
            private_key_copy['prime1'].native,
            private_key_copy['prime2'].native,
            private_key_copy['modulus'].native
        )
