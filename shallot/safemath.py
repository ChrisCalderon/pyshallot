from gmpy2 import invert as _invert, gcd as _gcd


def invert(n: int, m: int) -> int:
    """Wraps gmpy2.invert with a call to int"""
    return int(_invert(n, m))


def gcd(a: int, b: int) -> int:
    """Wraps gmyp2.gcd with a call to int"""
    return int(_gcd(a, b))
