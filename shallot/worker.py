import re
from shallot.keyops import gen_key_bases, check, encode_onion
from multiprocessing import Queue, Event
from shallot.safemath import gcd, invert

EMIN: int = 0x10001
EMAX: int = 0xFFFFFFFFFF


def worker(pattern: bytes, rsa_bits: int, result: Queue, stop: Event) -> None:
    pattern = re.compile(pattern, re.ASCII)
    match = pattern.match
    primes = gen_key_bases(rsa_bits)
    trials = 0

    for p, q, modulus in primes:
        totient = modulus - (p + q - 1)
        for public_exponent in range(EMIN, EMAX + 1, 2):
            if stop.is_set():
                result.put((b'NONE', b'NONE', trials))
                return
            trials += 1
            onion = encode_onion(modulus, public_exponent)
            if match(onion) and gcd(public_exponent, totient) == 1:
                private_exponent = invert(public_exponent, totient)
                private_key = check(
                    modulus,
                    public_exponent,
                    private_exponent,
                    p,
                    q
                )
                if private_key:
                    stop.set()
                result.put((onion, private_key, trials))
                return
