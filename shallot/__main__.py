from argparse import ArgumentParser, ArgumentTypeError
from multiprocessing import Queue, Event, Process, cpu_count
from shallot.worker import worker
import sys
import re


def valid_pattern(p: str) -> bytes:
    p = p.lower().encode()
    try:
        re.compile(p, re.ASCII)
    except re.error as e:
        raise ArgumentTypeError() from e

    return p


def main():
    parser = ArgumentParser(description='Generate vanity Tor domains.')
    parser.add_argument(
        '-p', '--pattern',
        help='The regular expression to match against.',
        type=valid_pattern,
        required=True
    )
    parser.add_argument(
        '-t', '--threads',
        help='Set the number of processes to use.',
        type=int,
        default=cpu_count(),
    )
    parser.add_argument(
        '-b', '--rsa-bits',
        help=(
            'Set the bit length of the RSA modulus. '
            'Tor does not support any size besides 1024!'
        ),
        type=int,
        default=1024
    )
    args = parser.parse_args()
    results = Queue()
    stop = Event()
    processes = []
    for i in range(args.threads):
        processes.append(Process(
            target=worker,
            args=(args.pattern, args.rsa_bits, results, stop)
        ))
        processes[i].start()

    onion, private_key, trials = None, None, 0
    for i in range(args.threads):
        result = results.get()
        if onion is None and result.onion != b'NONE':
            onion = result[0]
            private_key = result[1]
        trials += result[2]

    for p in processes:
        p.join()

    message = f'| Found match after {trials} tries: {onion.decode()}.onion |'
    box_bounds = '+' + (len(message) - 2)*'-' + '+'
    print(box_bounds)
    print(message)
    print(box_bounds)
    print()
    print(private_key.decode())
    return 0


if __name__ == '__main__':
    sys.exit(main())
