# PyShallot
A python program for finding pretty .onion domains.

To use, clone the repo and try `python -m shallot -h` to see the options.

## Benchmark
I ran the included benchmark on my 2018 MacBook Pro,
which has a 2.9GHz Core i9 CPU, with the option `-t 12`. Here 
are the results:

| Prefix length | Average time in seconds (10 trials) |
| ------------: | ----------------------------------: |
|             1 |                 0.15058291299999998 |
|             2 |                        0.1590680633 |
|             3 |                        0.5323781255 |
|             4 |                       10.2142832765 |
|             5 |                       299.886363981 |

This is about 100 times slower than the original C version.
